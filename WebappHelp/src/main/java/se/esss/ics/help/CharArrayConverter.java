package se.esss.ics.help;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * <code>CharArrayConverter</code> is managed session bean which implements
 * <code>Converter</code> interface and converts character arrays and strings.
 * 
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean(name = "charArrayConverter")
@SessionScoped
public class CharArrayConverter implements Converter, Serializable {

    private static final long serialVersionUID = 740865073466581382L;
    
    private static final String EMPTY_STRING = "";

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.String)
     */
    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
        if (submittedValue.trim().isEmpty()) {
            return null;
        } else {
            return submittedValue.toCharArray();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext,
     * javax.faces.component.UIComponent, java.lang.Object)
     */
    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null || String.valueOf(value).isEmpty()) {
            return EMPTY_STRING;
        } else {
            try {
                return String.valueOf((char[]) value);
            } catch (ClassCastException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error",
                        "Could not convert to char array."), e);
            }
        }
    }
}