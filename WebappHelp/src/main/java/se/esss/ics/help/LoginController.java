/*
 * Copyright (c) 2014 European Spallation Source
 * Copyright (c) 2014 Cosylab d.d.
 *
 * This file is part of Cable Database.
 * Cable Database is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or any newer version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
 */
package se.esss.ics.help;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import se.esss.ics.rbac.loginmodules.service.Message;
import se.esss.ics.rbac.loginmodules.service.RBACSSOSessionService;

/**
 * A UI controller bean for the login / logout button and form.
 *
 * @author <a href="mailto:miha.novak@cosylab.com">Miha Novak</a>
 */
@ManagedBean
@ViewScoped
public class LoginController implements Serializable {

    private static final long serialVersionUID = 5984791196886929176L;
    
    @Inject
    private RBACSSOSessionService sessionService;

    private String username;
    private char[] password;
    
    /** Opens login dialog. */
    public void openLoginDialog() {
    	RequestContext.getCurrentInstance().execute("PF('loginDialog').show()");
    }

    /** Performs login. */
    public void login() {
        try {
            final Message result = sessionService.login(username, new String(password));
            showMessage(result, "Sign In");
        } finally {
            clearPassword();
        }
    }

    /** Performs logout. */
    public void logout() {
        try {
            final Message result = sessionService.logout();
            showMessage(result, "Sign Out");
        } finally {
            clearPassword();
        }
    }

    /** Cancels login in progress. */
    public void cancel() {
        clearPassword();
    }

    /** @return the username */
    public String getUsername() {
        return username;
    }

    /** @param username the username to set */
    public void setUsername(String username) {
        this.username = username;
    }

    /** @return the password */
    public char[] getPassword() {
        return password;
    }

    /** @param password the password to set */
    public void setPassword(char[] password) {
        this.password = password;
    }
    
    /**
     * @return true if user is logged in otherwise false.
     */
    public boolean isLoggedIn() {
    	return sessionService.isLoggedIn();
    }
    
    /**
     * Returns the welcome message. Welcome message depends on whether a user is logged in.
     * 
     * @return the welcome message.
     */
    public String getWelcomeMessage() {
        if (!sessionService.isLoggedIn()) {
            return "Welcome, unknown visitor.";
        } else {
            return "Welcome, " + sessionService.getVisibleName() + ".";
        }
    }

    /**
     * Shows message.
     * 
     * @param result result
     * @param action action
     */
    private void showMessage(Message result, String action) {
        final Severity severity = result.isSuccessful() ? FacesMessage.SEVERITY_INFO : FacesMessage.SEVERITY_ERROR;
        final String summary = action + " " + (result.isSuccessful() ? "Succeeded" : "Failed");
        final String message = result.getMessage();
        final FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(severity, summary, message));
    }
    
    /**
     * Clears password.
     */
    private void clearPassword() {
        password = null;
    }
}